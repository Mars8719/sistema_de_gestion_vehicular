/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Codex_Cars;

/**
 *
 * @author 57301
 */
public class asignacion_Carreras {
    
    private Integer id;
    private String nombre;
    private String apellido;
    private String email;
    private Integer telefono;
    private String direccion;
    private Integer Km_Recorridos;
    private Integer id_conductor;
    private Integer id_vehiculo;

    public asignacion_Carreras() {
    }

    public asignacion_Carreras(Integer id, String nombre, String apellido, String email, Integer telefono, String direccion, Integer Km_Recorridos, Integer id_conductor, Integer id_vehiculo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.Km_Recorridos = Km_Recorridos;
        this.id_conductor = id_conductor;
        this.id_vehiculo = id_vehiculo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getKm_Recorridos() {
        return Km_Recorridos;
    }

    public void setKm_Recorridos(Integer Km_Recorridos) {
        this.Km_Recorridos = Km_Recorridos;
    }

    public Integer getId_conductor() {
        return id_conductor;
    }

    public void setId_conductor(Integer id_conductor) {
        this.id_conductor = id_conductor;
    }

    public Integer getId_vehiculo() {
        return id_vehiculo;
    }

    public void setId_vehiculo(Integer id_vehiculo) {
        this.id_vehiculo = id_vehiculo;
    }
    
    
    
    

    
    
    

    
    
}
