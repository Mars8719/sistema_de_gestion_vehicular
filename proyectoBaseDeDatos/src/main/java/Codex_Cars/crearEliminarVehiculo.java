/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Codex_Cars;

/**
 *
 * @author 57301
 */
public class crearEliminarVehiculo {
    
        private Integer id;
        private Integer id_propietario;
        private Integer id_conductor;
        private String placa;
        private String marca;
        private String modelo;
        private String año;
        private Integer capacidad;

    public crearEliminarVehiculo() {
    }

    public crearEliminarVehiculo(Integer id, Integer id_propietario, Integer id_conductor, String placa, String marca, String modelo, String año, Integer capacidad) {
        this.id = id;
        this.id_propietario = id_propietario;
        this.id_conductor = id_conductor;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.año = año;
        this.capacidad = capacidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_propietario() {
        return id_propietario;
    }

    public void setId_propietario(Integer id_propietario) {
        this.id_propietario = id_propietario;
    }

    public Integer getId_conductor() {
        return id_conductor;
    }

    public void setId_conductor(Integer id_conductor) {
        this.id_conductor = id_conductor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }


        
        
}
