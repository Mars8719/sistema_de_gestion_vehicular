/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Codex_Cars;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 57301
 */
public class Database {

    private final String usuario = "postgres";
    private final String password = "1987";
    private final String url = "jdbc:postgresql://localhost:5432/XamaCITransport";
    private Connection con;

    public boolean conectar() {
        con = null;
        try {
            con = DriverManager.getConnection(url, usuario, password);
            System.out.println("Conexion Exitosa");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public String iniciarSesion(String user, String pass) {

        PreparedStatement stmt;
        ResultSet rs;
        String query = "SELECT nombre,contraseña,rol FROM usuarios WHERE nombre = ? and contraseña = ? ";
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, user);
            stmt.setString(2, pass);
            rs = stmt.executeQuery();

            if (rs.next()) {
                String u = rs.getString("nombre");
                String p = rs.getString("contraseña");
                String r = rs.getString("rol");
                return r;
            } else {
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(inicioDeSesion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void cerrarConexion() {
        try {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean insertarCliente(String nombre, String apellido, String email, Integer telefono, String direccion, Integer Km_Recorrido) {
        String sql = "INSERT INTO clientes(nombre, apellido, email, telefono, direccion, Km_Recorrido) "
                + "VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.setInt(6, Km_Recorrido);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean actualizarCliente(Integer id, String nombre, String apellido, String email, Integer telefono, String direccion, Integer Km_Recorrido) {
        String sql = "UPDATE clientes SET "
                + "nombre = ?, "
                + "apellido = ?, "
                + "email = ?, "
                + "telefono = ?, "
                + "direccion = ?, "
                + "Km_Recorrido = ?"
                + "WHERE id_cliente = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.setInt(6, Km_Recorrido);
            ps.setInt(7, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean eliminarCliente(Integer id) {
        String sql = "DELETE FROM clientes "
                + "WHERE id_cliente = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<asignacion_Carreras> Consultarclientes() {
        List<asignacion_Carreras> res = new ArrayList<>();

        String sql = "SELECT * FROM clientes";
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            while (rs.next()) {
                asignacion_Carreras c = new asignacion_Carreras(
                        rs.getInt("id_cliente"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getString("email"),
                        rs.getInt("telefono"),
                        rs.getString("direccion"),
                        rs.getInt("Km_Recorrido"),
                        rs.getInt("id_conductor"),
                        rs.getInt("id_vehiculo"));
                res.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public boolean insertarVehiculoAsignado(Integer id_propietario, Integer id_conductor, Integer id_vehiculo) {
        String sql = "INSERT INTO vehiculo_asignado(id_propietario, id_conductor, id_vehiculo) "
                + "VALUES (?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id_propietario);
            ps.setInt(2, id_conductor);
            ps.setInt(3, id_vehiculo);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean actualizarVehiculoAsignado(Integer id, Integer id_propietario, Integer id_conductor, Integer id_vehiculo) {
        String sql = "UPDATE vehiculo_asignado SET "
                + "id_propietario = ?,"
                + "id_vehiculo = ?,"
                + "id_conductor = ?"
                + "WHERE id_vehiculo_asignado = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id_propietario);
            ps.setInt(2, id_conductor);
            ps.setInt(3, id_vehiculo);
            ps.setInt(4, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<asignar_Vehiculo> ConsultarVehiculosAsignado() {
        List<asignar_Vehiculo> ves = new ArrayList<>();

        String sql = "SELECT * FROM vehiculo_asignado";
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            while (rs.next()) {
                asignar_Vehiculo v = new asignar_Vehiculo(
                        rs.getInt("id_propietario"),
                        rs.getInt("id_conductor"),
                        rs.getInt("id_vehiculo"),
                        rs.getInt("id"));
                ves.add(v);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ves;
    }

    public boolean eliminarVehiculoAsignado(Integer id) {
        String sql = "DELETE FROM vehiculo_asignado "
                + "WHERE id_vehiculo_asignado = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
              ps.setInt(0, id);
//            ps.setInt(0, id_vehiculo);
//            ps.setInt(1, id_conductor);
//            ps.setInt(2, id_propietario);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean insertarConductor(String nombre, String apellido, String email, Integer telefono, String direccion) {
        String sql = "INSERT INTO conductores(nombre, apellido, email, telefono, direccion)"
                + "VALUES (?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean actualizarConductor(Integer id, String nombre, String apellido, String email, Integer telefono, String direccion) {
        String sql = "UPDATE conductores SET "
                + "nombre = ?,"
                + "apellido = ?,"
                + "email = ?,"
                + "telefono = ?,"
                + "direccion = ?"
                + "WHERE id_conductor = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.setInt(6, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<crearEliminarConductores> ConsultarConductor() {
        List<crearEliminarConductores> ces = new ArrayList<>();

        String sql = "SELECT * FROM conductores";
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            while (rs.next()) {
                crearEliminarConductores cs = new crearEliminarConductores(
                        rs.getInt("id_conductor"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getString("email"),
                        rs.getInt("telefono"),
                        rs.getString("direccion"));
                ces.add(cs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ces;
    }

    public boolean eliminarConductor(Integer id) {
        String sql = "DELETE  FROM conductores "
                + "WHERE id_conductor = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean insertarVehiculo(Integer id_propietario, Integer id_conductor, String placa, String marca, String modelo, String año, Integer capacidad) {
        String sql = "INSERT INTO vehiculos(placa, marca, modelo, _año, capacidad)"
                + "VALUES (?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, placa);
            ps.setString(2, marca);
            ps.setString(3, modelo);
            ps.setInt(4, Integer.parseInt(año));
            ps.setInt(5, capacidad);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean actualizarVehiculo(Integer id, Integer id_propietario, Integer id_conductor, String placa, String marca, String modelo, String año, Integer capacidad) {
        String sql = "UPDATE vehiculos SET "
                + "id_propietario = ?,"
                + "id_conductor = ?,"
                + "placa = ?,"
                + "marca = ?,"
                + "modelo = ?,"
                + "_año = ?,"
                + "capacidad = ?"
                + "WHERE id_vehiculo = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id_propietario);
            ps.setInt(2, id_conductor);
            ps.setString(3, placa);
            ps.setString(4, marca);
            ps.setString(5, modelo);
            ps.setString(6, año);
            ps.setInt(7, capacidad);
            ps.setInt(8, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<crearEliminarVehiculo> ConsultarVehiculo() {
        List<crearEliminarVehiculo> ves = new ArrayList<>();

        String sql = "SELECT * FROM vehiculos";
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            while (rs.next()) {
                crearEliminarVehiculo vs = new crearEliminarVehiculo(
                        rs.getInt("id_vehiculo"), +rs.getInt("id_conductor"),
                        rs.getInt("id_propietario"),
                        rs.getString("placa"),
                        rs.getString("marca"),
                        rs.getString("modelo"),
                        rs.getString("_año"),
                        rs.getInt("capacidad"));
                ves.add(vs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ves;
    }

    public boolean eliminarVehiculo(Integer id) {
        String sql = "DELETE  FROM vehiculos "
                + "WHERE id_vehiculo = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean insertarPropietario(String nombre, String apellido, String email, Integer telefono, String direccion) {
        String sql = "INSERT INTO propietarios(nombre, apellido, email, telefono, direccion)"
                + "VALUES (?,?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean actualizarPropietario(Integer id, String nombre, String apellido, String email, Integer telefono, String direccion) {
        String sql = "UPDATE propietarios SET "
                + "nombre = ?,"
                + "apellido = ?,"
                + "email = ?,"
                + "telefono = ?,"
                + "direccion = ?"
                + "WHERE id_propietario= ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, email);
            ps.setInt(4, telefono);
            ps.setString(5, direccion);
            ps.setInt(6, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<registroPropietario> ConsultarPropietario() {
        List<registroPropietario> pes = new ArrayList<>();

        String sql = "SELECT * FROM propietarios";
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            while (rs.next()) {
                registroPropietario ps = new registroPropietario(
                        rs.getInt("id_propietario"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getString("email"),
                        rs.getInt("telefono"),
                        rs.getString("direccion"));
                pes.add(ps);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pes;
    }
    public boolean eliminarPropietario(Integer id) {
        String sql = "DELETE  FROM propietarios "
                + "WHERE id_propietario = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
