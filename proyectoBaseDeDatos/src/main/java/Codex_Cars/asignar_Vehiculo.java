/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */
package Codex_Cars;

/**
 *
 * @author 57301
 */

public class asignar_Vehiculo {
    
    private Integer id_propietario;
    private Integer id_conductor;
    private Integer id_vehiculo;
    private Integer id;


    public asignar_Vehiculo() {
    }

    public asignar_Vehiculo(Integer id_propietario, Integer id_conductor, Integer id_vehiculo, Integer id) {
        this.id_propietario = id_propietario;
        this.id_conductor = id_conductor;
        this.id_vehiculo = id_vehiculo;
        this.id = id;
    }

    public Integer getId_propietario() {
        return id_propietario;
    }

    public void setId_propietario(Integer id_propietario) {
        this.id_propietario = id_propietario;
    }

    public Integer getId_conductor() {
        return id_conductor;
    }

    public void setId_conductor(Integer id_conductor) {
        this.id_conductor = id_conductor;
    }

    public Integer getId_vehiculo() {
        return id_vehiculo;
    }

    public void setId_vehiculo(Integer id_vehiculo) {
        this.id_vehiculo = id_vehiculo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    

   

   
    
    
    
    

}
